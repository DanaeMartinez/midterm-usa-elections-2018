# United States 2018 Midterm elections

After the the defeat of Democrats in 2016, the midterm election in 2018 was an opportunity to democrats to take control to the legislative branch, while the republicans looked for keep their voters engaged and strength their position. This small report aims to provide a brief insight about the voter turnout, the fundraising and the expenses made during the campaign as well as the election’s results.

The data comes from [The United States Elections Project](http://www.electproject.org/2018g), leaded by Dr. Michael P. McDonald.
The mission of the project is to provide timely and accurate election statistics, electoral laws, research reports, and other useful information regarding the United States electoral system. By providing this information, the project seeks to inform the people of the United States on how their electoral system works, how it may be improved, and how they can participate in it.

![view](images/turnout.png)

This markdown report has been build using plotly to visualize the results of the midterm USA elections of 2018. The used theme is the `hpstr` theme (from `prettydoc` package). The repport includes choropleth maps to identify the voter turnout by state. You can have acces to it via this [link](https://rpubs.com/DanaeMartinez/midterm_usa_elections).     

References:

1. [plotly cheatsheet](https://images.plot.ly/plotly-documentation/images/r_cheat_sheet.pdf?_ga=2.262472844.1660058896.1542557393-851051236.1530303515)
2. [Plotly R Graphing Library](https://plotly.com/r/)
3. [DataCamp cours: Interactive Data Visualization with plotly in R](https://learn.datacamp.com/courses/interactive-data-visualization-with-plotly-in-r)
4. [The United States Elections Project](http://www.electproject.org/2018g)
